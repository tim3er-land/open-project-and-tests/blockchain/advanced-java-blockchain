package pl.tim3erland.blockchain.advance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationEventPublisher;
import pl.tim3erland.blockchain.advance.blockchain.BlockchainService;
import pl.tim3erland.blockchain.advance.config.BlockchainConfig;
import pl.tim3erland.blockchain.advance.dto.Block;
import pl.tim3erland.blockchain.advance.miners.MinerService;

import java.util.LinkedList;
import java.util.List;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    BlockchainService blockchainService;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<MinerService> minerServiceList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            minerServiceList.add(addNewMinerService(i + 1));
        }

        for (int i = 0; i < 100; i++) {
            List<Thread> threads = new LinkedList<>();
            for (MinerService minerService : minerServiceList) {
                Thread test = new Thread(new Runnable() {
                    public void run() {
                        minerService.mineBlock("test");
                    }
                });
                threads.add(test);
            }
            for (Thread thread : threads) {
                thread.start();
            }
            boolean threadDone = false;
            while (!threadDone) {
                threadDone = true;
                for (Thread thread : threads) {
                    if (thread.isAlive()) {
                        threadDone = false;
                    }
                }
            }


        }
        List<Block> blocks = blockchainService.getBlockchain();
        System.out.println(blocks.size());
    }

    private MinerService addNewMinerService(int i) {
        return new MinerService(i, i, 1, blockchainService, applicationEventPublisher);
    }
}
