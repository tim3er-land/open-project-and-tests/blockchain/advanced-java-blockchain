package pl.tim3erland.blockchain.advance.blockchain;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pl.tim3erland.blockchain.advance.blockchain.event.BlockEvent;
import pl.tim3erland.blockchain.advance.dto.Block;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: Piotr Witowski
 * @date: 17.10.2021
 * @project pl.tim3erland.blockchain
 * Day of week: niedziela
 */
@Slf4j
@Service
public class BlockchainService {
    private List<Block> blocks;
    private Integer dificulty;

    public BlockchainService() {
        this.blocks = new ArrayList<>();
        this.dificulty = 1;
    }

    @EventListener
    public void receiveBlock(BlockEvent blockEvent) {
        String prevBlockHash = getPrevHashBlock();
        if (prevBlockHash.equalsIgnoreCase(blockEvent.getBlock().getPreHashBlock())) {
            log.info("Block {} mined by miner: {} with block hash: {}", blocks.size() + 1, blockEvent.getBlock().getMinerPublicKey(), blockEvent.getBlock().getHashBlock());
            blocks.add(blockEvent.getBlock());
            dificulty = (blocks.size() / 10) + 1;
        }
    }

    public String getPrevHashBlock() {
        String prevBlockHash = "";
        if (!CollectionUtils.isEmpty(blocks)) {
            prevBlockHash = blocks.get(blocks.size() - 1).getHashBlock();
        }
        return prevBlockHash;
    }

    public Integer getDificulty() {
        return this.dificulty;
    }

    public List<Block> getBlockchain() {
        return blocks;
    }
}
