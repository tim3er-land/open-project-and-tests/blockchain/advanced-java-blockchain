package pl.tim3erland.blockchain.advance.blockchain.event;

import lombok.Builder;
import lombok.Data;
import org.springframework.context.ApplicationEvent;
import pl.tim3erland.blockchain.advance.dto.Block;

import java.time.Clock;

/**
 * @author: Piotr Witowski
 * @date: 17.10.2021
 * @project pl.tim3erland.blockchain
 * Day of week: niedziela
 */
@Data
public class BlockEvent extends ApplicationEvent {
    private Block block;

    @Builder
    public BlockEvent(Object source, Block block) {
        super(source);
        this.block = block;
    }
}
