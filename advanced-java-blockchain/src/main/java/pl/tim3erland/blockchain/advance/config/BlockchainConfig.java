package pl.tim3erland.blockchain.advance.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import pl.tim3erland.blockchain.advance.blockchain.BlockchainService;
import pl.tim3erland.blockchain.advance.miners.MinerService;

/**
 * @author: Piotr Witowski
 * @date: 17.10.2021
 * @project pl.tim3erland.blockchain
 * Day of week: niedziela
 */
@Configuration
public class BlockchainConfig implements ApplicationContextAware {
    public static ApplicationContext applicationContext;

    @Bean
    public MinerService minerService(BlockchainService blockchainService, ApplicationEventPublisher applicationEventPublisher) {
        return new MinerService(0, 1, 1, blockchainService, applicationEventPublisher);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        BlockchainConfig.applicationContext = applicationContext;
    }
}
