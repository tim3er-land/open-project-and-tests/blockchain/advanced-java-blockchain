package pl.tim3erland.blockchain.advance.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author: Piotr Witowski
 * @date: 17.10.2021
 * @project pl.tim3erland.blockchain
 * Day of week: niedziela
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Block {
    private String preHashBlock;
    private String blockData;
    private LocalDateTime blockTimestamp;
    private String minerPublicKey;
    private Integer nonce;
    private String hashBlock;
}
