package pl.tim3erland.blockchain.advance.miners;

import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import pl.tim3erland.blockchain.advance.blockchain.BlockchainService;
import pl.tim3erland.blockchain.advance.blockchain.event.BlockEvent;
import pl.tim3erland.blockchain.advance.dto.Block;
import pl.tim3erland.blockchain.advance.utils.CryptoUtils;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.util.Base64;

/**
 * @author: Piotr Witowski
 * @date: 17.10.2021
 * @project pl.tim3erland.blockchain
 * Day of week: niedziela
 */
public class MinerService {
    private Integer noneInit;
    private Integer noneIncremense;
    private Integer wait;
    private BlockchainService blockchainService;
    private ApplicationEventPublisher applicationEventPublisher;
    private PrivateKey priv;
    private PublicKey pub;

    public MinerService(Integer noneInit, Integer noneIncremense, Integer wait, BlockchainService blockchainService, ApplicationEventPublisher applicationEventPublisher) {
        this.noneInit = noneInit;
        this.noneIncremense = noneIncremense;
        this.wait = wait;
        this.blockchainService = blockchainService;
        this.applicationEventPublisher = applicationEventPublisher;
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "SUN");
            KeyPair pair = keyGen.generateKeyPair();
            this.priv = pair.getPrivate();
            this.pub = pair.getPublic();
        } catch (Exception ex) {

        }
    }

    public void mineBlock(String blockData) {
        String prevHashBlock = blockchainService.getPrevHashBlock();
        Integer dificulty = blockchainService.getDificulty();
        Block.BlockBuilder blockBuilder = Block.builder()
                .minerPublicKey(Base64.getMimeEncoder().encodeToString(pub.getEncoded()))
                .blockTimestamp(LocalDateTime.now())
                .blockData(blockData)
                .preHashBlock(prevHashBlock);
        String startsDificulty = "";
        for (int i = 0; i < dificulty; i++) {
            startsDificulty += "0";
        }
        Integer nonce = this.noneInit;
        String blockHash = "";
        do {
            nonce += noneIncremense;
            blockBuilder.nonce(nonce);
            blockHash = CryptoUtils.calculateHash(blockBuilder.build());
        } while (!blockHash.startsWith(startsDificulty));
        applicationEventPublisher.publishEvent(
                BlockEvent.builder()
                        .block(blockBuilder.hashBlock(blockHash)
                .build())
                        .source(this)
                .build());
    }
}
