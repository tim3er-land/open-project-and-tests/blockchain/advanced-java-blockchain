package pl.tim3erland.blockchain.advance.utils;

import org.apache.commons.codec.binary.Hex;
import pl.tim3erland.blockchain.advance.dto.Block;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * @author: Piotr Witowski
 * @date: 17.10.2021
 * @project pl.tim3erland.blockchain
 * Day of week: niedziela
 */
public class CryptoUtils {
    public static String calculateHash(Block block) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] blockHex = getBlockHex(block);
            return new String(Hex.encodeHex(digest.digest(blockHex)));
        } catch (Exception ex) {
            return null;
        }
    }

    private static byte[] getBlockHex(Block block) {
        return ("" + block.getBlockData() + block.getPreHashBlock() + block.getBlockTimestamp() + block.getNonce()).getBytes(StandardCharsets.UTF_8);
    }
}
